FROM freqtradeorg/freqtrade:develop
USER root
RUN apt-get update \
    && apt-get -y install git build-essential \
    && apt-get clean \
    && pip install ta-lib \
    && pip install git+https://github.com/freqtrade/technical \
    && apt remove --purge -y build-essential \
    && apt autoremove -y 
USER ftuser
